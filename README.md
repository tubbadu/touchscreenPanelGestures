# touchscreenPanelGestures
A plasmoid to help manage windows and virtual desktops on a touchscreen

# install
```bash
cd ~/.local/share/plasma/plasmoids # navigate to the plasmoid folder
git clone https://codeberg.org/tubbadu/touchscreenPanelGestures # clone this repo
mv touchscreenPanelGestures org.kde.plasma.touchscreenPanelGestures # rename it to match the module's name
```

## setup

- create a top panel, set it as "Always visible" so that windows won't cover it
- place this plasmoid in the middle of the top panel
- install [this kwin script](https://codeberg.org/tubbadu/TouchscreenVirtualDesktopManager), that will automatically place each new window in a new virtual desktop
- enable the kwin script in the settings
- (optional) set the virtual desktop switcher animation as "slide" for better visual feel

## usage

- sliding with the finger on the top panel toward right or left will change virtual desktop
    - you can change more than one virtual desktop with a single slide, if you slide long enough
- tapping on the plasmoid will trigger the `ExposeAll` effect, presenting you all the windows
- in the plasmoid settings you can change the sliding and tapping threesholds

## known issues and todos
- [ ] sliding from the plasmoid toward the bottom of the screen ("vertically") will swap desktop even if the threeshold has not been reached
- [ ] add more customization options (like custom clock format string)
- [ ] write a better readme
- [ ] add a better way to install
- [ ] add a store.kde.org package

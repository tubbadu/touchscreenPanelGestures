import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.3
import org.kde.kirigami 2.4 as Kirigami

Kirigami.FormLayout {
	id: page

	property alias cfg_timeThreeshold: timeThreeshold.value
	property alias cfg_xThreeshold: xThreeshold.value



	///////////////////////////
	RowLayout{
		Label{
			text: i18n("Tap time threeshold (in ms):")
		}
		SpinBox {
			id: timeThreeshold
			from: 1
			to: 1000
		}
	}
	RowLayout{
		Label{
			text: i18n("Dragging threeshold (in px):")
		}
		SpinBox {
			id: xThreeshold
			from: 1
			to: 5000
		}
	}
}

/*
 *   SPDX-FileCopyrightText: 2022 Tubbadu <tubbadu@gmail.com>
 *
 *   SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.3

import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0



Item {
	id: root
	property int startingX: 0
	property int initialStartingX: 0
	property int xThreeshold: Plasmoid.configuration.xThreeshold //100
	property double startingTime: 0
	property int timeThreeshold: Plasmoid.configuration.timeThreeshold //100 // in milliseconds
	property int longPressTimeThreeshold: 300 // TODO add in config
	property int longPressXThreeshold: 50 // TODO add in config
	property bool disableLongPress: false
	anchors.fill: parent

	PlasmaCore.DataSource {
        id: executable
        engine: "executable"
        connectedSources: []
        onNewData: disconnectSource(sourceName)

        function exec(cmd) {
            executable.connectSource(cmd)
        }
    }

    PlasmaCore.ToolTipArea { // documentation here: https://api.kde.org/frameworks-api/frameworks-apidocs/frameworks/plasma-framework/html/classToolTip.html // edit: dead link
		id: tooltip
		timeout: -1
		anchors.fill: parent
		enabled: false
	}

    function goToDesktopRight(){
		//executable.exec("qdbus org.kde.KWin /KWin nextDesktop");
		executable.exec("qdbus org.kde.KWin /KWin setCurrentDesktop $(expr $(qdbus org.kde.KWin /KWin currentDesktop) + 1)");
	}
	function goToDesktopLeft(){
		//executable.exec("qdbus org.kde.KWin /KWin previousDesktop");
		executable.exec("qdbus org.kde.KWin /KWin setCurrentDesktop $(expr $(qdbus org.kde.KWin /KWin currentDesktop) - 1)");
	}

	function exposeWindows(){
		executable.exec("qdbus org.kde.kglobalaccel /component/kwin invokeShortcut ExposeAll");
	}

	function raiseKeyboard(){
		executable.exec("qdbus org.kde.KWin /VirtualKeyboard forceActivate");
	}

	Plasmoid.preferredRepresentation: Plasmoid.compactRepresentation

	Plasmoid.compactRepresentation: RowLayout {
		Layout.fillWidth: true
		Layout.fillHeight: true

		/*RowLayout{
			id: leftToolbar
			Layout.fillHeight: true
		}*/

		MultiPointTouchArea {
			id: touch

			Layout.fillWidth: true
			Layout.fillHeight: true

			touchPoints: [
				TouchPoint { id: p1 }
			]

			onPressed: {
				startingX = -1; //p1.x;
			}

			onUpdated: {
				if(startingX == -1){
					startingX = p1.x;
					initialStartingX = startingX;
					startingTime = Date.now();
					disableLongPress = false;
				}
				let deltaX = p1.x - startingX;
				//label.text = p1.x + ", " + startingX + ", " + deltaX
				if(deltaX > xThreeshold){
					// sliding right
					startingX = p1.x
					goToDesktopLeft()
				} else if(-deltaX > xThreeshold) {
					// sliding left
					startingX = p1.x
					goToDesktopRight();
				} else {
					//doNothing()
				}

				// check for long press
				if(Math.abs(p1.x - initialStartingX) < longPressXThreeshold){
					if(Date.now() - startingTime > longPressTimeThreeshold && !disableLongPress){
						// long press happened
						exposeWindows() // TODO do something different
					}
				} else {
					disableLongPress = true;
				}
			}

			onReleased: {
				if(Date.now() - startingTime < timeThreeshold && !disableLongPress) {
					// single tap / click
					exposeWindows()
				} else {
					// pressed for too long
					// TODO add a long press action (perhaps in onUpdated, not here)
				}
			}

			Item {
				id: mainCompactRepresentation
				anchors.fill: parent
				/*Rectangle{
					id: xx
					color: "green"
					anchors.fill: parent
					Text{
						id: log
						color: "pink"
						//text: startingTime
					}
				}*/
				Label {
					id: label
					text: Qt.formatDateTime(clock.getCurrentTime(), clock.datetimeFormat)
					anchors.centerIn: parent
				}
			}

			Item {
				id: clock

				property string datetimeFormat: "ddd d MMM - HH:mm"

				function getCurrentTime() {
					// get the time for the given timezone from the dataengine
					var now = dataSource.data["it-IT"]["DateTime"];
					// get current UTC time
					var msUTC = now.getTime() + (now.getTimezoneOffset() * 60000);
					// add the dataengine TZ offset to it
					var currentTime = new Date(msUTC + (dataSource.data["it-IT"]["Offset"] * 1000));

					return currentTime;
				}

				PlasmaCore.DataSource {
					id: dataSource
					engine: "time"
					connectedSources: ["it-IT"]
					interval: 60000
					intervalAlignment: PlasmaCore.Types.AlignToMinute
				}
			}
		}
		RowLayout {
			id: rightToolbar
			Layout.fillHeight: true

			PlasmaCore.IconItem{
				anchors.fill: parent
				source: "input-keyboard-virtual"

				MouseArea {
					anchors.fill: parent
					onClicked: {
						raiseKeyboard()
					}
				}
			}
		}
	}
}
